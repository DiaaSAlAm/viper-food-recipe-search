//
//  Helper.swift
//  Food Recipe Search
//
//  Created by mac on 8/6/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import Foundation

public class Helper {
    
    /// Singletone instance
    public static let instance: Helper = Helper()
    
    // singletone
    private init(){}
    
    
    enum ControllerID: String{
         case LoginVC = "LoginVC"
         case UserOrdersNavController = "UserOrdersNavController"
         case ComingOrdersNavController = "ComingOrdersNavController"
         case ContainerVC = "ContainerVC"
         case HomeVC = "HomeVC"
         case VideoChatVC = "VideoChatVC"
       }
    
    enum storyboardName: String{
        case Authentication = "Authentication"
        case Main = "Main"
    }
    
    enum cells: String {
        case recentSearchCell = "RecentSearchCell"
        case searchResultCell = "SearchResultCell"
    }
    
   var recentSearchText : [String] {
        get {
            return prefs.stringArray(forKey: UD.PrefKeys.recentSearchText) ?? []
        }
        set {
            prefs.set(newValue, forKey: UD.PrefKeys.recentSearchText)
        }
    }
    
}


