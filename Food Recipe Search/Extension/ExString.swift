//
//  ExString.swift
//  Food Recipe Search
//
//  Created by mac on 8/6/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import Foundation

extension String {
    func isEmptyOrWhitespace() -> Bool { //Check if text is empty or with white Soaces "    "
        if(self.isEmpty) {
            return true
        }
        return (self.trimmingCharacters(in: NSCharacterSet.whitespaces) == "")
    }
}
