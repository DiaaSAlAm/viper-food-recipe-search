//
//  SearchVC.swift
//  Food Recipe Search
//
//  Created by mac on 8/6/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import UIKit

class SearchVC: UIViewController, SearchViewProtocol {
    
    //MARK: - IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    //MARK: - Properties
    var presenter:  SearchPresenterProtocol!
    var selectedSearchType: DataType = .recentSearch
    private let recentSearchCell = Helper.cells.recentSearchCell.rawValue
    private let searchResultCell = Helper.cells.searchResultCell.rawValue
    private let activityView = UIActivityIndicatorView(style: .gray)
    private let fadeView: UIView = UIView()
    private var width : CGFloat = CGFloat()
    var searchText: String = ""
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func showLoadingIndicator() {
        startAnimatingActivityView()
    }
    
    func hideLoadingIndicator() {
        stopAnimatingActivityView()
    }
     
    func showToastMessage(message: String) {
        let width = self.view.frame.size.width
        let toastLabel = UILabel(frame: CGRect(x: (width/4) , y: self.view.frame.size.height-100, width: width / 2, height: 35))
        
        toastLabel.backgroundColor = UIColor.red.withAlphaComponent(0.6)
        toastLabel.numberOfLines = 0
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 5.0, delay: 0.2, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    func reloadData() {
        selectedSearchType = .resultSearch
        presenter.handleCollectionState(numberOfItems: presenter.numberOfRowsSearch, width: width , collectionView: collectionView)
        collectionView.reloadData()
    }
    
}

//MARK: - UI Setup
extension SearchVC {
    
    fileprivate func setupUI() {
        title = "Recipe Search"
        searchBar.delegate = self
        width = self.view.frame.width - 16
        let searchBarStyle = searchBar.value(forKey: "searchField") as? UITextField
        searchBarStyle?.clearButtonMode = .never
        hideKeyboardWhenTappedAround()
        registerCollectionView()
    }
    
    //MARK: - Start Animating Activity
    fileprivate func startAnimatingActivityView() {
        fadeView.frame = self.collectionView.frame
        fadeView.backgroundColor = .white
        fadeView.alpha = 0.6
        self.view.addSubview(fadeView)
        self.view.addSubview(activityView)
        activityView.hidesWhenStopped = true
        activityView.center = self.collectionView.center
        activityView.startAnimating()
    }
    
    //MARK: - Stop Animating Activity
    fileprivate func stopAnimatingActivityView() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 1, delay: 1, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                self.collectionView?.alpha = 1
                self.fadeView.removeFromSuperview()
                self.activityView.stopAnimating()
            }, completion: nil)
        }
    }
    
    //MARK: - Register  CollectionView Cell
    fileprivate func registerCollectionView(){
        collectionView.register(UINib(nibName: recentSearchCell, bundle: nil), forCellWithReuseIdentifier: recentSearchCell)
        collectionView.register(UINib(nibName: searchResultCell, bundle: nil), forCellWithReuseIdentifier: searchResultCell) 
    }
    
}


//MARK: - UICollectionView Delegate
extension SearchVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        defer { collectionView.deselectItem(at: indexPath, animated: true) }
        switch selectedSearchType {
        case .recentSearch:
            print("recentSearch")
            self.searchText = presenter.getRecentSearch(indexPath.row)
            presenter.insertNewRecipe_To_RecentSearch(recentSearchText: searchText)
            searchBar.text = searchText //Change Search bar test to selected item text
            presenter.viewDidLoad()
        default:
             let model = ResultDetailsModel(recipeImage: presenter.getResultImage(indexPath.row), recipeTitle: presenter.getRecipeTitle(indexPath.row), recipeIngredients: presenter.getRecipeIngredients(indexPath.row), recipeLink: presenter.getRecipeLink(indexPath.row))
            presenter.goToResultDetails(data: model)
            break
        }
    }
}

//MARK: - UICollectionView Data Source
extension SearchVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch selectedSearchType {
        case .recentSearch:
            return presenter.numberOfRowsRecentSearch
        case .resultSearch:
            return presenter.numberOfRowsSearch
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch selectedSearchType {
        case .recentSearch:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: recentSearchCell,for: indexPath) as! RecentSearchCell
            presenter.configureRecentSearch(cell: cell, indexPath: indexPath)
            return cell
        case .resultSearch:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: searchResultCell,for: indexPath) as! SearchResultCell
             presenter.configureSearchResult(cell: cell, indexPath: indexPath) 
            return cell
        }
    }
}

//MARK: - UICollectionView Delegate Flow Layout
extension SearchVC: UICollectionViewDelegateFlowLayout {
    
    //MARK: - Load new item if Available
    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        if selectedSearchType == .resultSearch, presenter.more && indexPath.item == presenter.numberOfRowsSearch - 1 {
            presenter.fetchNextPage()
        }
        
    }
    
    //MARK: - change size For Item Dependence on CollectionView type
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
        switch selectedSearchType {
        case .recentSearch:
            return CGSize(width: width - 16, height: 40)
        case .resultSearch:
            return CGSize(width: width - 16, height: 345)
        }
    }
}

//MARK: Search Bar Delegate
extension SearchVC : UISearchBarDelegate {
    
    //MARK: - Search Button Clicked
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchBarText = searchBar.text, !searchBar.text!.isEmptyOrWhitespace() else {return}
        presenter.insertNewRecipe_To_RecentSearch(recentSearchText: searchBarText)
        self.searchText = searchBarText
        selectedSearchType = .resultSearch 
        presenter.viewDidLoad()
        view.endEditing(true) //Dissmiss Keyboard
        
    }
    
    //MARK: - Cancel Button Clicked -> Reload Last Data Fetched
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
         selectedSearchType = .resultSearch
        self.collectionView.reloadData()
    }
    
    //MARK: - SearchBar Text Did Begin Editing -> Show Recent Suggestions
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        selectedSearchType = .recentSearch
        presenter.handleCollectionState(numberOfItems: presenter.numberOfRowsRecentSearch, width: width , collectionView: collectionView)
        self.collectionView.reloadData()
    }
}

 

