//
//  SearchModel.swift
//  Food Recipe Search
//
//  Created by mac on 8/6/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//
 
 
import Foundation

struct SearchModel : Codable {
    let q : String
    let from : Int
    let to : Int
    let more : Bool
    let count : Int
    let hits : [Hits] 
}
 
struct SearchResponseError : Codable { 
    let status: String
    let message: String
}

struct Hits : Codable {
    let recipe : Recipe
    let bookmarked : Bool
    let bought : Bool
}

struct Recipe : Codable {
    let label : String
    let image : String
    let source : String
    let url : String
    let healthLabels : [String]
    let ingredientLines : [String]
}

struct SearchResultViewModel {
    var label: String
    var image: String
    var scource: String
    var recipeHealthLabels: String
    
    init(data: Hits) {
        label =  data.recipe.label
        image =  data.recipe.image
        scource = data.recipe.source
        recipeHealthLabels = data.recipe.healthLabels.joined(separator: ", ") 
    }
}

struct RecentSearchViewModel {
    var recentSearchLabel: String
    
    init(data: String) {
        recentSearchLabel = data
    }
    
}
