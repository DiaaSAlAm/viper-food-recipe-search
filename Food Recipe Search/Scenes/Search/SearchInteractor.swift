//
//  SearchInteractor.swift
//  Food Recipe Search
//
//  Created by mac on 8/6/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import Foundation
import Alamofire

class SearchInteractor: SearchInteractorInputProtocol {

    weak var presenter: SearchInteractorOutputProtocol?
    private var searchModel : SearchModel?
    private var hitsResponse : [Hits] = [] 

    
    func fetchSearchRecipeResult(searchText: String, from: Int, to: Int, append: Bool) {
         Alamofire.request(APIRouter.search(searchText: searchText, from: from, to: to))
             .responseJSON {[weak self] (response) in 
                 guard let self = self else { return }
                 if let jsonData = response.data {
                     let jsonDecoder = JSONDecoder()
                     do {
                         let model = try jsonDecoder.decode(SearchModel.self, from: jsonData)
                         self.searchModel = model
                        self.hitsResponse = append ? (self.hitsResponse + model.hits) : model.hits
                        self.presenter?.dataFetchedSuccessfuly(hits: self.hitsResponse, searchModel: self.searchModel!)
                     }catch{
                         do {
                              let model = try jsonDecoder.decode(SearchResponseError.self, from: jsonData)
                            self.presenter?.dataFetchingFailed(withError: model.message)
                         }catch{
                            self.presenter?.dataFetchingFailed(withError: "Connection Error")
                         }
                     }
              }
         }
     }
    
}
