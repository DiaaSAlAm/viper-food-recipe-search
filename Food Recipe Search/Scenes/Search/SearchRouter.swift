//
//  SearchRouter.swift
//  Food Recipe Search
//
//  Created by mac on 8/6/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import UIKit

 
class SearchRouter: SearchRouterProtocol {
    
    func goToResultDetails(data: ResultDetailsModel) {
        print("Go TO")
        let view = UIStoryboard(name: "ResultDetails", bundle: nil).instantiateViewController(withIdentifier: "ResultDetailsVC") as! ResultDetailsVC
          let interactor =  ResultDetailsInteractor(resultDetailsModel: data)
          let router =  ResultDetailsRouter()
          let presenter =  ResultDetailsPresenter(view: view, interactor: interactor, router: router)
          view.presenter = presenter
          interactor.presenter = presenter
//          interactor.getData()
          router.viewController = view
        self.viewController?.navigationController?.pushViewController(view, animated: true)
    }
    //Router is only one take dicret instance

    weak var viewController: UIViewController?

    static func createModule() -> UIViewController {
        let view = UIStoryboard(name: "Search", bundle: nil).instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
        let interactor =  SearchInteractor()
        let router =  SearchRouter()
        let presenter =  SearchPresenter(view: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view
        return view
    }

}
