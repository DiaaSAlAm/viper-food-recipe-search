//
//  SearchPresenter.swift
//  Food Recipe Search
//
//  Created by mac on 8/6/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import UIKit

class SearchPresenter: SearchPresenterProtocol,  SearchInteractorOutputProtocol {
    
    
    
    weak var view: SearchViewProtocol?
    private let interactor: SearchInteractorInputProtocol
    private let router: SearchRouterProtocol
    private var searchResultHits = [Hits]()
    private var searchModel: SearchModel?
    
    var more: Bool {
        return searchModel?.more ?? false
    }
    var numberOfRowsSearch: Int {
        return searchResultHits.count
    }
       
    var numberOfRowsRecentSearch: Int {
        return Helper.instance.recentSearchText.count
    } 
    
    init(view: SearchViewProtocol, interactor: SearchInteractorInputProtocol, router: SearchRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    
    func viewDidLoad() {
           print("viewDidLoad")
        view?.showLoadingIndicator()
        interactor.fetchSearchRecipeResult(searchText: view?.searchText ?? "", from: 0, to: 10 , append: false)
    }
    
    func fetchNextPage() {
        view?.showLoadingIndicator()
        interactor.fetchSearchRecipeResult(searchText: view?.searchText ?? "", from: (searchModel?.from ?? 0), to: (searchModel?.to ?? 0) + 10 , append: true)
    }
    
    func dataFetchedSuccessfuly(hits: [Hits], searchModel: SearchModel) {
        print("usersFetchedSuccessfuly")
        view?.hideLoadingIndicator()
        self.searchResultHits = hits
        self.searchModel = searchModel
        view?.reloadData()
     }
     
     func dataFetchingFailed(withError error: String) {
          print("usersFetchingFailed \(error)")
          view?.hideLoadingIndicator()
          view?.showToastMessage(message: error)
     }
    
    func insertNewRecipe_To_RecentSearch(recentSearchText : String){
        
        if let recipeIndex = Helper.instance.recentSearchText.firstIndex(of: recentSearchText) { // Replce elment index from element postion to first index
            Helper.instance.recentSearchText.remove(at: recipeIndex)
        }
        
        Helper.instance.recentSearchText.insert(recentSearchText, at: 0) // insert elment at 0 index
        
        if numberOfRowsRecentSearch > 10 { // Show last 10 recent Search
            Helper.instance.recentSearchText.removeLast()
        }
        
        saveRecentSearchOnUserDefaults() // Save Recent Suggetions on User Defaults
        
    }
    
    func saveRecentSearchOnUserDefaults(){
        prefs.set(Helper.instance.recentSearchText, forKey: UD.PrefKeys.recentSearchText)
    }
    
    func getRecentSearch(_ row: Int) -> String {
        return Helper.instance.recentSearchText.getElement(at: row) ?? ""
    }
    
    
    //MARK: Search Result Data
    func getResultImage(_ row: Int) -> String {
        return searchResultHits.getElement(at: row)?.recipe.image ?? ""
    }
    
    func getRecipeTitle(_ row: Int) -> String {
        return searchResultHits.getElement(at: row)?.recipe.label ?? ""
    }
    
    func getRecipeIngredients(_ row: Int) -> String {
        return searchResultHits.getElement(at: row)?.recipe.ingredientLines.joined(separator: " \n") ?? ""
    }
    
    func getRecipeLink(_ row: Int) -> String {
        return searchResultHits.getElement(at: row)?.recipe.url ?? ""
    }
    
    //MARK: - handle label on background collectionView
    func handleCollectionState(numberOfItems: Int,width: CGFloat, collectionView: UICollectionView) {
        if numberOfItems == 0 {
            let frame = CGRect(x: 0, y: 0, width: width, height: 30)
            let label = UILabel(frame: frame)
            label.text = "No Data Available"
            label.textColor = .darkGray
            label.textAlignment = .center
            label.center = collectionView.center
            label.sizeToFit()
            collectionView.backgroundView = label
        } else {
            collectionView.backgroundView = nil
        }
    }
    
    func configureSearchResult(cell: SearchResultCellViewProtocol, indexPath: IndexPath) {
        guard let hits = self.searchResultHits.getElement(at: indexPath.row) else { return }
        let viewModel = SearchResultViewModel(data: hits)
        cell.configureSearchResult(viewModel: viewModel)
    }
    
    func configureRecentSearch(cell: RecentSearchCellViewProtocol, indexPath: IndexPath) {
        guard let recentSearch = Helper.instance.recentSearchText.getElement(at: indexPath.row) else {return }
        let viewModel = RecentSearchViewModel(data: recentSearch)
        cell.configureRecentSearch(viewModel: viewModel)
    }
    
    func goToResultDetails(data: ResultDetailsModel) {
           print("GO TO [Presnter]")
        router.goToResultDetails(data: data)
       }
    
} 
