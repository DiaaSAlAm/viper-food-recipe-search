//
//  SearchResultCell.swift
//  Food Recipe Search
//
//  Created by mac on 8/6/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import UIKit

class SearchResultCell: UICollectionViewCell, SearchResultCellViewProtocol {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var resultImageView: UIImageView!
    @IBOutlet weak var recipeTitle: UILabel!
    @IBOutlet weak var scource: UILabel!
    @IBOutlet weak var recipeHealthLabels: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureSearchResult(viewModel: SearchResultViewModel) {
       resultImageView.setImage(with: viewModel.image)
       recipeTitle.text = viewModel.label
       scource.text = viewModel.scource
       recipeHealthLabels.text = viewModel.recipeHealthLabels 
   }

}
