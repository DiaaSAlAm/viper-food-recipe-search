//
//  RecentSearchCell.swift
//  Food Recipe Search
//
//  Created by mac on 8/6/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import UIKit

class RecentSearchCell: UICollectionViewCell, RecentSearchCellViewProtocol { 

    @IBOutlet weak var recentSearchLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureRecentSearch(viewModel: RecentSearchViewModel) {
        recentSearchLabel.text = viewModel.recentSearchLabel
    }

}
