//
//  Search.swift
//  Food Recipe Search
//
//  Created by mac on 8/6/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import UIKit

enum DataType {
   case recentSearch
   case resultSearch 
}

protocol SearchViewProtocol: class { //View Conteroller
    var presenter: SearchPresenterProtocol! { get set }
    var searchText: String { get set } 
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func showToastMessage(message: String)
    func reloadData()
}

protocol SearchPresenterProtocol: class { // Logic
    var view: SearchViewProtocol? { get set } 
    var numberOfRowsSearch: Int { get }
    var numberOfRowsRecentSearch: Int { get }
    var more: Bool  { get }
    func viewDidLoad()
    func fetchNextPage()
    func insertNewRecipe_To_RecentSearch(recentSearchText: String)
    func saveRecentSearchOnUserDefaults()
    func getRecentSearch(_ row: Int) -> String
    func getResultImage(_ row: Int) -> String
    func getRecipeTitle(_ row: Int) -> String
    func getRecipeIngredients(_ row: Int) -> String
    func getRecipeLink(_ row: Int) -> String 
    func goToResultDetails(data: ResultDetailsModel)
    func handleCollectionState(numberOfItems: Int,width: CGFloat, collectionView: UICollectionView)
    func configureSearchResult(cell: SearchResultCellViewProtocol, indexPath: IndexPath)
    func configureRecentSearch(cell: RecentSearchCellViewProtocol, indexPath: IndexPath)
    
}


protocol SearchRouterProtocol { // For Segue
    func goToResultDetails(data: ResultDetailsModel)
}

protocol SearchInteractorInputProtocol: class { // func do it from presenter
     var presenter: SearchInteractorOutputProtocol? { get set }
     func fetchSearchRecipeResult(searchText: String, from: Int, to: Int, append: Bool)
}

protocol SearchInteractorOutputProtocol: class { // it's will call when interactor finished
    func dataFetchedSuccessfuly(hits: [Hits], searchModel: SearchModel)
    func dataFetchingFailed(withError error: String)
}

protocol SearchResultCellViewProtocol {
    func configureSearchResult(viewModel: SearchResultViewModel)
}

protocol RecentSearchCellViewProtocol {
    func configureRecentSearch(viewModel: RecentSearchViewModel)
}
