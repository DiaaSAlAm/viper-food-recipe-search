//
//  ResultDetailsPresenter.swift
//  Food Recipe Search
//
//  Created by mac on 8/9/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import UIKit

class ResultDetailsPresenter: ResultDetailsPresenterProtocol, ResultDetailsInteractorOutputProtocol { 
      
    weak var view:  ResultDetailsViewProtocol?
    private let interactor: ResultDetailsInteractorInputProtocol
    private let router: ResultDetailsRouterProtocol
    private var resultDetailsModel: ResultDetailsModel? 
    
    var recipeImage: String {
        return resultDetailsModel?.recipeImage ?? ""
    }
    
    var recipeTitle: String {
        return resultDetailsModel?.recipeTitle ?? ""
    }
    
    var recipeIngredients: String {
        return resultDetailsModel?.recipeIngredients ?? ""
    }
    
    var recipeLink: String {
        return resultDetailsModel?.recipeLink ?? ""
    }
    
    init(view:  ResultDetailsViewProtocol, interactor:  ResultDetailsInteractorInputProtocol, router: ResultDetailsRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    
    func viewDidLoad() {
        interactor.getData()
    }
    
    func dataFetchedSuccessfuly(data: ResultDetailsModel) {
        resultDetailsModel = data
        view?.reloadData()
    }
    
    func dataFetchingFailed(withError error: String) { 
        view?.showToastMessage(message: error)
    }
    
} 
