//
//  ResultDetailsInteractor.swift
//  Food Recipe Search
//
//  Created by mac on 8/9/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import Foundation

class ResultDetailsInteractor: ResultDetailsInteractorInputProtocol {

    weak var presenter: ResultDetailsInteractorOutputProtocol?
    private var resultDetailsModel : ResultDetailsModel?
    
    init(resultDetailsModel : ResultDetailsModel) {
        self.resultDetailsModel = resultDetailsModel
    }
    
   func getData() {
    if resultDetailsModel != nil {
        self.presenter?.dataFetchedSuccessfuly(data: resultDetailsModel!)
    }else{
        self.presenter?.dataFetchingFailed(withError: "Data Not Found!")
     } 
   }
}
