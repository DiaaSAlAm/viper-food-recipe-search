//
//  ResultDetailsVC.swift
//  Food Recipe Search
//
//  Created by mac on 8/9/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import UIKit
import SafariServices

class ResultDetailsVC: UIViewController, ResultDetailsViewProtocol {
    
    //MARK: - IBOutlets
    @IBOutlet private weak var recipeImage: UIImageView!
    @IBOutlet private weak var recipeTitle: UILabel!
    @IBOutlet private weak var recipeIngredients: UILabel!
    @IBOutlet private weak var linkButton: UIButton!
    
    //MARK: - Properties
    var presenter:  ResultDetailsPresenterProtocol!
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        presenter.viewDidLoad()
    } 
    
    func reloadData() { 
        setData()
    }
    
    func showToastMessage(message: String) {
        let width = self.view.frame.size.width
        let toastLabel = UILabel(frame: CGRect(x: (width/4) , y: self.view.frame.size.height-100, width: width / 2, height: 35))
        
        toastLabel.backgroundColor = UIColor.red.withAlphaComponent(0.6)
        toastLabel.numberOfLines = 0
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 5.0, delay: 0.2, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    //MARK: - IBOutlets
    @IBAction
    func didPressedLinkButton(_ sender: UIButton) {
        openSafariViewController()
    }
    
}

//MARK: Setup View
extension ResultDetailsVC {
    
    fileprivate func setupUI() {
        title = "Item Details"
        linkButton.layer.cornerRadius = linkButton.frame.height / 2
     }
       
    fileprivate func setData(){
     recipeImage.setImage(with: presenter.recipeImage  )
        recipeTitle.text = presenter.recipeTitle
        recipeIngredients.text = presenter.recipeIngredients
    }
    
    fileprivate func openSafariViewController(){
        guard let url = URL(string: presenter.recipeLink) else { return }
        let svc = SFSafariViewController(url: url)
        present(svc, animated: true, completion: nil)
    }
     
}
