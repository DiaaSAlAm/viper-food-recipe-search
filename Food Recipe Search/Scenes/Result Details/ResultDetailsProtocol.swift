//
//  ResultDetails.swift
//  Food Recipe Search
//
//  Created by mac on 8/9/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import Foundation 


protocol ResultDetailsViewProtocol: class { //View Conteroller
    var presenter: ResultDetailsPresenterProtocol! { get set }
    func showToastMessage(message: String)
    func reloadData()
}

protocol ResultDetailsPresenterProtocol: class { // Logic
    var view: ResultDetailsViewProtocol? { get set } 
    func viewDidLoad()
    var recipeImage: String { get }
    var recipeTitle: String { get }
    var recipeLink: String { get }
    var recipeIngredients: String { get }
}


protocol ResultDetailsRouterProtocol { // For Segue
    
}

protocol ResultDetailsInteractorInputProtocol: class { // func do it from presenter
     var presenter: ResultDetailsInteractorOutputProtocol? { get set }
    func getData()
}

protocol ResultDetailsInteractorOutputProtocol: class { // it's will call when interactor finished
    func dataFetchedSuccessfuly(data: ResultDetailsModel)
    func dataFetchingFailed(withError error: String)
}
 
