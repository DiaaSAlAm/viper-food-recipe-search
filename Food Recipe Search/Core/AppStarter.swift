//
//  AppStarter.swift
//  Food Recipe Search
//
//  Created by mac on 8/6/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import UIKit

/// AppStarter here you can handle everything before letting your app starts
final class AppStarter {
    static let shared = AppStarter()
    
    private init() {}
    
    func start(window: UIWindow?) { 
        AppTheme.apply()
        setRootViewController(window: window)
    }
    
    private func setRootViewController(window: UIWindow?) { 
        let rootViewController = UINavigationController(rootViewController: SearchRouter.createModule())
        window?.rootViewController = rootViewController
        window?.makeKeyAndVisible()
    }
}
